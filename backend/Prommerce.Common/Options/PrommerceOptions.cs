﻿namespace Prommerce.Common.Options;

public class PrommerceOptions
{
    public Environments? Environments { get; set; }
    public Cors? Cors { get; set; }
}

public class Environments
{
    public string? WebHostURL { get; set; }
}

public class Cors
{
    public string? WebHostPolicy { get; set; }
}