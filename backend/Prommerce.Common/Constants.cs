﻿namespace Prommerce.Common;

public class Constants
{
    public enum Genders
    {
        Male = 1,
        Female,
        Binary,
        Other,
        NotSpecified
    }
}