using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Prommerce.API;
using Prommerce.API.Initializer;
using Prommerce.Application.RouteHandlers;

var builder = WebApplication.CreateBuilder(args);

var services = builder.Services;
var configuration = builder.Configuration;

var corsPolicy = new CorsPolicyBuilder(configuration["PrommerceOptions:Environments:WebHostURL"])
    .AllowAnyHeader()
    .AllowAnyMethod()
    .Build();

services.AddCors(options =>
{
    options.AddPolicy(configuration["PrommerceOptions:Cors:WebHostPolicy"], corsPolicy);
});

services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer();
services.AddAuthorization(options =>
{
    options.FallbackPolicy = new AuthorizationPolicyBuilder()
    .AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme)
    .RequireAuthenticatedUser()
    .Build();
});

//services.AddAuthorizationBuilder()
//    .AddPolicy("admin", policy =>
//    policy.RequireRole("admin").RequireClaim("permission", "admin"));

services.AddEndpointsApiExplorer();
services.AddSwaggerGen(options =>
{
    //options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
    //{
    //    Description = "",
    //    Name = "Authorization",
    //    In = ParameterLocation.Header,
    //    Type = SecuritySchemeType.ApiKey
    //});
    //options.AddSecurityRequirement(new OpenApiSecurityRequirement
    //{
    //    {
    //        new OpenApiSecurityScheme
    //        {
    //            Reference = new OpenApiReference
    //            {
    //                Id = "Bearer",
    //                Type = ReferenceType.SecurityScheme
    //            }
    //        },
    //        new List<string>()
    //    }
    //});
});

//services.AddSwaggerGen(options =>
//{
//    options.OperationFilter<XmlOperationFilter>();
//});
//services.Configure<SwaggerGeneratorOptions>(options =>
//{
//    options.InferSecuritySchemes = true;
//});

services.AddPrommerceServices(configuration);

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI(options =>
    {
        options.RoutePrefix = "";
    });
}

app.UseHttpsRedirection();
app.UseRouting();
app.UseCors(configuration["PrommerceOptions:Cors:WebHostPolicy"]);
app.UseAuthentication();
app.UseAuthorization();
app.MapApplicationEndpoints();

// Initializers
using (var scope = app.Services.CreateScope())
{
    try
    {
        app.Logger.LogInformation("Executing initializers");
        var initializers = scope.ServiceProvider.GetServices<IInitializer>();
        foreach (var initializer in initializers.OrderByDescending(x => x.Priority))
        {
            app.Logger.LogInformation($"Executing initializer {initializer.GetType().Name}");
            await initializer.Initialise();
        }
    }
    catch (Exception e)
    {
        app.Logger.LogError(e, "Initializer failed");
        throw;
    }
}

static EndpointFilterDelegate RequestAuditor(EndpointFilterFactoryContext handlerContext, EndpointFilterDelegate next)
{
    var loggerFactory = handlerContext.ApplicationServices.GetService<ILoggerFactory>();
    var logger = loggerFactory.CreateLogger("RequestAuditor");
    return (invocationContext) =>
    {
        logger.LogInformation($"Received a request for: {invocationContext.HttpContext.Request.Path}");
        return next(invocationContext);
    };
}

app.Run();