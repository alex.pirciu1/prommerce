﻿using Prommerce.API.Initializer;
using Prommerce.Application;
using Prommerce.Common.Options;
using Prommerce.Data;

namespace Prommerce.API;

public static class DependencyInjection
{
    public static IServiceCollection AddPrommerceServices(this IServiceCollection services, IConfiguration configuration)
    {
        // Inject services from other projects
        services.AddData();
        services.AddApplication();

        // Inject services in this project
        services.AddScoped<IInitializer, DbInitializer>();

        // Inject configuration options
        services.Configure<PrommerceOptions>(configuration.GetSection("PrommerceOptions"));

        return services;
    }
}