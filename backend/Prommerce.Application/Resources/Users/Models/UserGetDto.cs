﻿namespace Prommerce.Application.Resources.Users.Models;

public class UserGetDto : UserDto
{
    public Guid Id { get; set; }
}